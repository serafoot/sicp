; abs value 
(define (abs x)
  (cond ((> x 0) x)
        ((= x 0) 0)
        ((< x 0) (- x))))

(define (abs-if x)
  (if (< x 0)
      (- x)
      x))

; define an operator greater than/equal
(define (>= x y)
  (or (> x y)
      (= x y)))

; ex 1.3
(define (sum-biggest x y z)
  (cond ((and (> x y) (> y z)) (sum-square x y))
        ((and (> x y) (> z y)) (sum-square x z))
        ( else (sum-square y z))))
(define (sum-square x y)
  (+ (* x x) (* y y)))

; computing the square root using the newton algorithm
(define (sqrt x)
   (sqrt-iter 1.0 x))
(define (sqrt-iter guess x)
   (if (good-enough? guess x)
       guess
       (sqrt-iter (improve guess x) x)))
(define (good-enough? guess x)
   (< (abs (- (square guess) x)) 0.00000000001))
(define (improve guess x)
   (average guess (/ x guess)))
(define (square x)
   (* x x))
(define (average x y) 
   (/ (+ x y) 2))

; variant of good-enough?
(define (sqrt1 x)
   (sqrt-iter1 1.0 0 x))
(define (sqrt-iter1 guess pguess x)
   (if (good-enough1? guess pguess)
       guess
       (sqrt-iter1 (improve guess x) guess x)))
(define (good-enough1? guess pguess)
   (< (abs (- guess pguess)) 0.0000001))


; factorial with a linear recursive specification
(define (factorial n)
   (if (= n 1)
       1
       (* n (factorial (- n 1)))))

; Great Common Divisor (Euclid's Algorithm)
(define (gcd a b)
   (if (= b 0)
       a 
       (gcd b (remainder a b))))

; compute the smallest divisor
(define (smallest-divisor n) (find-divisor n 2))
(define (find-divisor n test-divisor)
   (cond ((> (square test-divisor) n) n)
         ((divides? test-divisor n) test-divisor)
         (else (find-divisor n (+ test-divisor 1)))))
(define (divides? a b) (= (remainder b a) 0))

; test if a number is a prime number
(define (prime? n)
   (= n (smallest-divisor n)))

; now let's use a result from Fermat's little theorem to check        
; if a number is a prime number.
(define (expmod base exp m)
   (cond ((= exp 0) 1)
         ((even? exp)
          (remainder 
           (square (expmod base (/ exp 2) m))
           m))
         (else 
           (remainder 
            (* base (expmod base (- exp 1) m))
            m))))
(define (fermat-test n)
   (define (try-it a)
      (= (expmod a n n) a))
   (try-it (+ 1 (random (- n 1)))))
(define (fast-prime? n times)
   (cond ((=  times 0) true)
         ((fermat-test n) (fast-prime? n (- times 1)))
         (else false))) 
