; High order procedures are procedures that have 
; procedure as arguments and/or return procedures
; as output


; define the sum of integers
(define (sum-integers a b)
	(if (> a b)
	0
	(+ a (sum-integers (+ a 1) b))))

; define the sum of cubes of integers
(define (sum-cubes a b) 
	(if (> a b)
	0 
	(+ (cube a) 
	   (sum-cubes (+ a 1) b))))	
(define (cube x) 
  (* x x x))

(define (pi-sum a b)
   (if (> a b)
       0
       (+ (/ 1.0 (* a (+ a 2))) 
          (pi-sum (+ a 4) b))))

; All the three previous procedures share some common
; structure: they are all summations 
; what is different is the <term> of the summation
; and how to compute the <next> element in the sum

(define (sum term a next b)
   (if (> a b)
       0 
       (+ (term a ) 
          (sum term (next a) next b))))

;  another example computing the integral betwenn a and b
(define (integral f a b dx)
   (define (add-dx x)
      (+ x dx))
   (* (sum f (+ a (/ dx 2.0)) add-dx b) dx))

; lambda, to avoid creating two procedures that we
; use just once in the code (anonymous procedures?)

(define (pi-sum a b)
   (sum (lambda (x) (/ 1.0 (* x (+ x 2))))
        a
	(lambda (x) (+ x 4))
	b))

(define (integral f a b dx)
   (* (sum f
           (+ a (/ dx 2.0))
           (lambda (x) (+ x dx))
           b)
       dx))


; lambda procedures are essentially qhat we do when 
; we define a procedure 
(define (plus4 x) (+ x 4))
; is equivalent to
(define plus4 (lambda (x) (+ x 4)))


; using let to create local variables
(define (f x y)
   (let ((a (+ 1 (* x y)))
         (b (- 1 y)))
    (+ (* x (square a))
       (* y b)
       (* a b))))


; implementation of the bi-section method
(define (search f neg-point pos-point)
   (let ((midpoint (average neg-point pos-point)))
   (if (close-enough? neg-point pos-point)
       midpoint
       (let ((test-value (f midpoint)))
         (cond ((positive? test-value)
	        (search f neg-point midpoint))
	       ((negative? test-value)
	        (search f midpoint pos-point))
	       (else midpoint))))))
(define (close-enough? x y) (< (abs (- x y)) 0.001))
(define (half-interval-method f a b)
   (let ((a-value (f a))
         (b-value (f b)))
   (cond ((and (negative? a-value) (positive? b-value))
          (search f a b))
         ((and (negative? b-value) (positive? a-value))
	  (search f b a))
	 (else
	  (error "Values are not of opposite signe" a b)))))

