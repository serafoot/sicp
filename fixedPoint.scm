(load "chap1")
(load "highOrder")
; This function compute the fixed point 
; of a function 

(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2))
       tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

; compute the square root using the logic 
; of fixed-point
(define (sqrt x)
  (fixed-point (lambda (y) (average y (/ x y)))
               1.0))

; compute the golden ratio as fixed-point (ex 1.35)
(define (gold-ratio)
  (fixed-point (lambda (x) (+ 1 (/ 1 x))) 2.0))

(define (cube-root x)
  (fixed-point (averge-damp (lambda (y) (/ x (square y))))
               1.0))

; let's implement the Newton's method
; first we need to define the concept of
; derivative
(define (deriv g)
  (lambda (x) (/ (- (g (+ x dx)) (g x)) dx)))

(define dx 0.00001)

(define (newton-transform g)
  (lambda (x) (- x (/ (g x) ((deriv g) x)))))
(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))

; let us define the sqrt procedure using 
; the newton's method
(define (sqrt x)
  (newtons-method
    (lambda (y) (- (square y) x)) 1.0))
